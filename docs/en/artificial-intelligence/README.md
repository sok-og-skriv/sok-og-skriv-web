---
title: AI in education
date: "2024-10-183"
tags:
  - KI
  - AI
  - ChatGPT
  - Artificial intelligence
---

# ![](/images/illustrasjoner_KI_500x450.png) AI in education

**How can AI tools (such as ChatGPT and Microsoft Copilot) be used?**

The main focus for Search & Write is writing, searching, study skills, and source use in higher education. When it comes to AI, the main focus of Search & Write then is how large language models, such as ChatGPT, can be used in such contexts.

AI tools such as ChatGPT and Microsoft Copilot are made to generate text, not to retrieve information. The tools are based on large langauge models that guess the next word of a sentence. They are trained on texts that contain a lot of information, but are not trained to retrieve information accurately. The tools serve better to enhance textual flow and continuity than to provide reliable content. 

The tools are developing very rapidly, making it difficult to draw up guidelines and legislation at the same pace. The Søk & Skriv editorial team provides general recommendations for safe and purposeful use, but you should always also check the guidelines at your institution and in your professional community.

 <Figure
  src="/images/AI ethics_januar 25.png" 
  alt="[Be critical and double-check facts, keep in mind that AI tools are resource-intensive, AI is just an aid, write, read, and think for yourself.]"
  caption=""
  type=""
/>

**Legal and ethical guidelines**

When regarding the use of AI, you must adhere to both legal and ethical guidelines, and you must consider how suitable, safe, and reliable using the tool is.

Remember that there are other methods and tools that can serve your purpose better. A search in Oria will for instance give you better access to relevant and quality checked sources than Microsoft Copilot.

Guidelines may vary between institutions and disciplines, and you should always check with your academic community. 

::: eksempel Be aware!
Remember to follow the relevant guidelines incorporated in the [EU AI Act](https://digital-strategy.ec.europa.eu/en/policies/regulatory-framework-ai), [GDPR](https://gdpr.eu/), and [intellectual property rights](https://lovdata.no/dokument/NL/lov/2018-06-15-40) (the Norwegian law of copyright from Lovdata in Norwegian) .
:::

Also, be mindful of the information you enter into an AI chat. Many students have access to their own chat services from their institution, such as GPT UiO, UiB chat, or Sikt AI chat. When you use these tools, you can share so-called green and yellow data. You must always avoid sharing confident and strictly confident information. Below you will see an explanation of what is meant by different forms of data:

<Figure
  src="/images/How to classify data and information.png" 
  alt="[Do not share sensitive information]"
  caption="Click on the image to read"
  type=""
/>

Note that you must never share material that is subject to copyright with open tools such as ChatGPT. The content from the material you enter can potentially be included in the tool and thereby become accessible to others who use it. This is considered a violation of copyright laws. You can read more about [the use of AI tools and copyright here](https://www.uio.no/english/services/ai/guidelines.html).

**Help when assessing AI tools**

Artificial intelligence has added new dimentions to the information flow. At the same time, it creates a growing need for critical assessment and navigation. The checklist below can help you analyze different AI tools, and give you an idea whether it serves your purpose, or not. 

::: tip Checklist: Evaluating AI Tools

Before using an AI tool, it might be wise to determine if the tool is right for you by asking a few questions:

1. **Does the tool meet your needs?** By examining the tool’s background and applications, and seeking others’ experiences, you can form an opinion on whether this particular tool is the one you should learn to use.
2. **Who created the tool?** If you find out who is behind the tool, how long they have been working on it, and their background, you can form an opinion on aspects such as quality, target audience, experience, and the tool’s purpose.
3. **What happens to your data?** When using AI tools, you often provide text, images, or other data. Your institution may have strict rules and guidelines on what you are allowed to share, and you must consider the copyright of the data you share. Is your data used to train the AI tool? Does someone profit from your data? If you do not know what happens to your data, you should not use the tool.
:::


:::details These questions are based on a more comprehensive checklist that you also can use:
| **Comprehensive checklist**                                                                                                                       |
| ---------------------------------------------------------------------------------------------------------------------------------------------     |
| **What is the purpose of the tool?** Can it make the work more efficient? Can it improve the quality of the work?                                 |
| **Who is the target audience?** Is the tool user-friendly? Does it require extensive training? Which academic fields does the tool support?       |
| **What is the tool good for?** What are its strengths and weaknesses based on its purpose? Is it useful for multiple stages of your work process? |
| **Who is the tool’s developer/provider?**                                                                                                         |
| **What sources does the tool use?** What types of data are extracted? What format must/can the data be in?                                        |
| **Does the tool handle sensitive data?**                                                                                                          |
| **Are there any ethical concerns regarding the tool?**                                                                                            |
| **Is it necessary to create an account?**                                                                                                         |
| **Does the tool run in the cloud, or locally on the machine?**                                                                                    |
| **Does the tool have open source code?**                                                                                                          |
| **Is future maintenance of the tool likely?**                                                                                                     |
:::

:::warning Exercise your own judgement
AI tools are resource intensive. Take this into account when evaluating the usefulness of the tool.
:::


## Searching

There are many different tools based on artificial intelligence that have been developed (and continue to be developed!) to find relevant soures, such as [Keenious](https://keenious.com/), [Elicit](https://elicit.com/), [ResearchRabbit](https://www.researchrabbit.ai/) and others. These tools can be useful if you are looking to find a few useful articles regarding a paper topic or research question during the initial stage of a search, or when you are doing literature searches that *supply* (are in addition to) an already conducted literature search. These tools are by themselves not suited for [systematic literature searches](https://www.sokogskriv.no/en/searching/systematic-searching.html), as these searches will not be able to satisfy the demand of being [verifiable](https://www.sokogskriv.no/en/searching/systematic-searching.html#how-to-do-systematic-searching).

[AI tools such as ChatGPT and Microsoft Copilot](https://www.sokogskriv.no/en/sources-and-referencing/different-sources.html#ai-tools) can be useful when you are getting started with a search strategy, as they can come with simple suggestions for how to set up a search for a research question. Be aware that the suggestions from such AI tools often can contain mistakes. It is important to be critical and carefully check for mistakes such as wrong search words, typos, or the inaccurate use of AND/OR. To ensure the quality of these search strategies, you must be knowledgeable of [academic literature searches](https://www.sokogskriv.no/en/searching/) and the discipline you work within. 

If you want to test out AI tools either to help you find [synonyms for an academic term](https://www.sokogskriv.no/en/searching/searching-techniques.html#free-text-searching), or get started with your [search strategy](https://www.sokogskriv.no/en/searching/searching-techniques.html), or come up with suggestions for relevant [academic articles](https://www.sokogskriv.no/en/sources-and-referencing/different-sources.html#the-scientific-article), it is important that you give the tool well articulated instructions (also known as [prompting](https://www.uio.no/english/services/ai/student/index.html), see below "Explore and Use") where you define the criteria for what you are looking for.

:::tip Handy tip 
Check with a subject librarian at your institution for good advice and help to find and use quality discovery tools when looking for academic literature for your paper. 
::: 

## Writing

In an academic context, it is crucial to rely on sources with a responsible author. As a student in an academic setting, this also obligates you to take responsibility as the author of the texts you write.

Evaluating the assessments and claims in a text is particularly important when using AI in the writing process. This is because AI-generated text, unlike a textbook or scholarly article, does not have a responsible author. Therefore, you must do all the work yourself. For example, if you ask a reliable AI tool to summarize the argument in a primary source, it is essential that you also verify whether this actually aligns with the primary source.

To assess whether you are using AI tools in a way that preserves your ownership of the text, you can try answering ‘yes’ or ‘no’ to these questions:

<Figure
  src="/images/flytskjema eng januar 25.png" 
  alt="[Is it crucial that the result is true? If so, it is not safe to use AI-generated content.]"
  caption=""
  type=""
/>

Generative AI like ChatGPT excels at pattern recognition in large datasets. This means it is reasonably reliable as a tool for identifying regularities and deviations in text, such as spelling, sentence structure, and common arrangements. This is particularly true for languages that are well-represented in the training material.

:::warning Excercise your own judgement
Text-generating AI tools do not provide reliable answers to factual questions, or to complex questions or to questions that imply assessment and require a rich understanding of context. These questions should still be answered by consulting academic literature, or discussed with your supervisor and fellow students.
:::

AI tools can be useful for supporting writing processes and developing writing skills, especially if you are writing in your second or third language and/or have reading and writing difficulties. The tool becomes more useful the more you learn to talk (chat) with it. The point is not to ‘get answers’ or finished text, but to use the language model to improve your own text, in a language that makes good sense to a reader.

::: details Here are some examples on how you can work:
- Ask the chatbot to rewrite smaller parts of your text into simpler/more direct/clearer language.
- Then ask it to explain what it did (for example, in what way the new text is simpler/clearer, etc.). The explanation will be useful feedback for you as a writer.
- In a third step, you can ask it to justify its choices. The chatbot will then explain why the sentence structure, word choice, etc., it has chosen is easier to understand.
- Ultimately, you must decide whether the ‘new’ language is something you want to use, and if you recognize your own voice in it, or if you prefer to keep the original text.

If you use language models in this way, it is somewhat similar to using existing tools like dictionaries (e.g., in Word), Google Translate, and online searches for phrases. However, it is generally good practice to disclose which AI tools you have used and how you have used them. This helps the reader of your text be more aware of biases and errors that only text-generating AI tools might make.

::: eksempel Be aware!
If you use ChatGPT or similar language models to produce finished text for submission, exams, etc., this could be considered cheating. It is important that you check the guidelines for your subject and educational institution.
:::

## Sources

**Text from AI tools (ChatGPT and Microsoft Copilot)**

Text from AI tools is not considered a source in an academic context. A central requirement for sources in this context is that they must have a responsible sender, such as one or more authors or an organization. This is not the case for texts generated by AI tools. Consequently, text generators like ChatGPT, Microsoft Copilot, and similar cannot be credited as co-authors. AI tools are considered software, and therefore, only in special cases should references to text from AI tools be included in the bibliography. Since such tools influence the outcome of your writing process, it is still good practice to explain which tools you have used and how you have used them.

::: tip Tips
A text generated by AI cannot be recreated by others, and thus, it cannot be verified. In your assignment, you should therefore describe how you have used the AI tool.
:::

When explaining your usage, it may be relevant to include what you entered into the chat, including the time, scope, and how the result was integrated into your text.

::: warning Exercise your own judgement
You should always verify and assess whether text from AI tools like ChatGPT and Microsoft Copilot is suitable for your purpose.
:::

**When should you cite AI-tools?**

You should reference AI tools if the tool you have used influences the results, analyses, and findings in your assignment, and when you create content with a tool to illustrate how it works. Refer in the usual way with in-text citations and provide the complete reference in the bibliography. In some contexts, it may also be relevant to include the response from the chat as an appendix in your assignment

**How do you cite AI-tools?**

The [APA 7th reference style](https://apastyle.apa.org/blog/how-to-cite-chatgpt) has made a recommendation on how to cite AI-generated texts.

::: eksempel Be aware!
Texts generated by artificial intelligence, such as large language models, are associated with ethical issues, and their use in an academic context may fall under regulations regarding cheating and plagiarism. Check what is allowed in your study program if you are considering using such a service.
:::

## Study skills

It is important to remember that AI can at best be a useful tool or aid for your studies, and not a replacement for [reading or writing yourself](/en/study-skills/). In the form below, you will find some useful tips on how you can use AI as a tool to become a better student.

| Skills     | Tasks                                                                                                                                                                                                                                        |
| ---------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Create     | Ask AI to arrange a schedule for a longer project, e.g. a term paper.                                                                                                                                                                        |
| Evaluate   | Assign AI the role as critic or constructive mentor that gives you feedback on your written text, e.g. a discussion or analysis.                                                                                                             |
| Analyse    | Ask AI to provide scenarios where you play the part as a professional diagnosing a condition, solving a legal issue, etc. You can also ask the AI to argue aganst you, acting as a sparring partner.                                         |
| Apply      | Assign AI the role as a fellow student that can help you with brainstorming and outlining an assignment you are going to write.                                                                                                              |
| Understand | Give AI the role as interested listener. Explain to AI how you have understood a lecture or a text.\* If you notice that some concepts are hard to understand or connections are unclear, you can ask AI to provide definitions og examples. |
| Remember   | Ask AI to provide you with control questions to check that you can reproduce the content you have read.    

The table is inpired by a resource from [Oregon State University](https://ecampus.oregonstate.edu/faculty/artificial-intelligence-tools/meaningful-learning/)

::: eksempel Be aware!
Do not share copyrighted material with [open AI tools](/en/artificial-intelligence/).
:::

## External resources


 + [UiO AI](https://www.uio.no/english/services/ai/index.html)

 + [UiB AI](https://www.uib.no/en/student/166996/tools-based-artificial-intelligence-education)

 + [HVL AI](https://www.hvl.no/en/kunstig-intelligens-ki/)

 + [PhD on Track: AI](https://www.phdontrack.net/open-science/AI.html)

 + [EU AI ACT](https://digital-strategy.ec.europa.eu/en/policies/regulatory-framework-ai)

 + [Digdir AI resources](https://www.digdir.no/kunstig-intelligens/ki-ressurser/4145) (in Norwegian with English links)

