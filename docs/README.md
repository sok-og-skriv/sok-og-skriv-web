---
home: true
footer: Søk & Skriv - Creative Commons Attribution-NonCommercial-ShareAlike 4.0
---

<div class="cards">
  <div class="card">
    <div class="image">
      <a href="/soking/"><img src="/images/illustrasjoner_sok_500x450.png" alt=""></a>
    </div>
    <div class="content">
      <h2><a href="/soking/">Søking</a></h2>
      <p>Få hjelp til å søke etter informasjon og komme i gang med oppgaveskriving</p>
    </div>
  </div>

  <div class="card">
    <div class="image">
      <a href="/skriving/"><img src="/images/illustrasjoner_skriving_500x450.png" alt=""></a>
    </div>
    <div class="content">
      <h2><a href="/skriving/">Skriving</a></h2>
      <p>I akademisk skriving stilles det bestemte krav til form og innhold, språk og stil</p>
    </div>
  </div>

  <div class="card" lang="nn">
    <div class="image">
      <a href="/kjeldebruk/"><img src="/images/illustrasjoner_kildehenvisning_500x450.png" alt=""></a>
    </div>
    <div class="content">
      <h2><a href="/kjeldebruk/">Kjeldebruk</a></h2>
      <p>All forsking bygger på kjelder. Få hjelp til å vurdere relevans, kvalitet og korleis unngå plagiat</p>
    </div>
  </div> 

  <div class="card">
    <div class="image">
      <a href="/studieteknikk/"><img src="/images/illustrasjoner_lesing_500x450.png" alt=""></a>
    </div>
    <div class="content">
      <h2><a href="/studieteknikk/">Studieteknikk</a></h2>
      <p>Her får du tips om hvordan du kan planlegge studiene, lese og forstå tekster, ta gode notater, samarbeide og jobbe i studiegrupper</p>
    </div>
  </div>

</div>
  
<div class="flex-cards">
 <div class="card">
    <div class="image">
      <a href="/referansestiler/"><img src="/images/illustrasjoner_referansestilar_500x450.png" alt=""></a>
    </div>
    <div class="content">
      <h2><a href="/referansestiler/">Referansestiler</a></h2>
      <p>
        <span class="tags"> 
          <span class="tag"><a href="/referansestiler/apa-7th.html">APA 7th</a></span>
          <span class="tag"><a href="/referansestiler/chicago-forfatter-aar.html">Chicago forfatter-år</a></span>
          <span class="tag"><a href="/referansestiler/chicago-fotnoter.html">Chicago fotnoter</a></span>
          <span class="tag"><a href="/referansestiler/harvard.html">Harvard</a></span>
          <span class="tag"><a href="/referansestiler/mla.html">MLA</a></span>
          <span class="tag"><a href="/referansestiler/vancouver.html">Vancouver</a></span>
        </span>
      </p>
    </div>
  </div>

  <div class="card">
    <div class="image">
      <a href="/frasebank/"><img src="/images/illustrasjoner_frasebank_500x450.png" alt=""></a>
    </div>
    <div class="content">
      <h2><a href="/frasebank/">Frasebank</a></h2>
      <p>Her finner du eksempler på uttrykk og vendinger som er mye brukt i akademiske tekster</p>
    </div>
  </div>

  <div class="card">
    <div class="image">
      <a href="/kunstig-intelligens/">
        <img src="/images/illustrasjoner_KI_500x450.png" alt="">
      </a>
    </div>
    <div class="content">
      <h2>
        <a href="/kunstig-intelligens/">
          KI i utdanning
        </a>
      </h2>
      <p>Oppdatert om bruk av kunstig intelligens i søking, kildebruk, skriving og studieteknikk</p>
    </div>
  </div>

  <div class="card">
    <div class="image">
      <a href="/ofte-stilte-sporsmal/">
        <img src="/images/illustrasjoner_faq_500x450.png" alt="">
      </a>
    </div>
    <div class="content">
      <h2>
        <a href="/ofte-stilte-sporsmal/">
          Ofte stilte spørsmål
        </a>
      </h2>
      <p>Finn svar på det alle andre lurer på!</p>
    </div>
  </div>
</div>

<section style="margin-bottom:3em;">
  <div class="flex">
    <h2 style="border-bottom:none;">Aktuelle videoer</h2>
    <small><a href="https://www.youtube.com/@skogskriv">...se flere</a></small>
  </div>
  <div style="aspect-ratio: 9/6 auto;">
  <iframe width="100%" height="100%" src="https://www.youtube-nocookie.com/embed/videoseries?si=QjO-y9omTKh_JbL_&amp;list=PLrn0Z-Bf8orosc-cbtBxhHNgvJZOc7Mbl" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
  </div>
</section>

## Partnere

<div class="partners">
  <div class="partner">
    <a href="https://www.hvl.no" class="no-external-link-icon">
      <picture>
        <source media="(max-width: 719px)" srcset="/partners/hvl-icon.jpg">
        <img src="/partners/hvl-logo.jpg" alt="Høyskolen på Vestlandet"/>
      </picture>
    </a>
    <div class="title">
      Høgskulen på Vestlandet
    </div>
  </div>
  <div class="partner">
    <a href="https://www.uib.no" class="no-external-link-icon">
      <picture>
        <source media="(max-width: 719px)" srcset="/partners/uib-icon.png">
        <img src="/partners/uib-logo.png" alt="Universitetet i Bergen"/>
      </picture>
    </a>
    <div class="title">
      Universitetet i Bergen
    </div>
  </div>
  <div class="partner">
    <a href="https://www.uio.no" class="no-external-link-icon">
      <picture>
        <source media="(max-width: 719px)" type="image/svg+xml" srcset="/partners/04_uio_segl_pos.svg">
        <img src="/partners/uio-logo.svg" alt="Universitetet i Oslo"/>
      </picture>
    </a>
    <div class="title">
      Universitetet i Oslo
    </div>
  </div>
</div>

--- 

<div class="container two-column footer-links">
  <div class="align-right">
    <div><a href="/om/">Om Søk & Skriv</a></div>
    <div><a href="/om/kontaktinformasjon.html">Kontaktinformasjon</a></div>
    <div><a href="/ofte-stilte-sporsmal/">Ofte stilte spørsmål</a></div>
    <div><a href="/om/sok-og-skriv-i-undervisning.html">Søk & Skriv i undervisning</a></div>
    <div><a href="https://uustatus.no/nb/erklaringer/publisert/c6d9a394-b5ac-48fd-9f1f-0154b2daacbc">Tilgjengelighetserklæring</a></div>
  </div>
  <div class="align-left">
    <div><a href="/en/about/">About Search & Write</a></div>
    <div><a href="/en/about/contact-information.html">Contact</a></div>
  </div>
</div>
