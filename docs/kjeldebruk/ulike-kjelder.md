---
title: "Ulike kjelder"
date: "2020-04-28"
lang: "nn"
next: "../studieteknikk/"
tags: 
  - "Vitenskapelig kilde"
  - "Vitskapleg kjelde"
  - Nettside
  - "Grå littertur"
  - Fagbok
  - Lærebok
  - "Vitenskapelige artikkel"
  - "Vitskaplege artikkel"
  - Antologi
  - Rapport
  - KI
  - AI
  - "Kunstig intelligens" 
  - ChatGPT
  - Handbooks
  - Leksikon
  - Doktorgradsavhandling
---
# Ulike kjelder

Her finn du eksempel på akademiske kjelder og andre kjelder som er vanlege å bruke i akademiske tekstar. Kjeldene er delt inn etter kva formål dei er eigna til.  

 ### Kva er formålet med kjelda? 

I tillegg til å vurdere om ei [kjelde er relevant og påliteleg](/kjeldebruk/kjeldevurdering.html), bør du og vurdere om kjelda eignar seg til det du skal bruke ho til. Ulike kjelder kan vere nyttige i ulike delar av skriveprosessen.  

I idémyldringa rundt tema for ein tekst kan du gjerne bruke kjelder som ikkje følger faglege krav til kjeldevurdering, som for eksempel nyheiter, kronikkar og KI-generert tekst.  

Nokre faglege kjelder er godt eigna når du skal sette deg inn i eit nytt emne for å få oversikt og bli kjent med fagfeltet og omgrepa som blir brukt, altså fagterminologien. Då kan du for eksempel bruke lærebøker eller oppslagsverk. Desse kjeldene er nyttige når du skal tolke eller formulere ei problemstilling i teksten, og når du skal skrive introduksjonen til oppgåva di. 

Når du seinare i skriveprosessen skal greie ut om, drøfte eller analysere eit tema, bør du og sjå etter kjelder som presenterer ny kunnskap, og som er godkjent av ekspertar i faget. Denne typen kjelder er med andre ord [fagfellevurdert](/kjeldebruk/ulike-kjelder.html#fagfellevurdering-peer-review) og reknast som byggesteinane i alt vitskapleg arbeid. Dei er derfor godt eigna til å bygge opp under argumentasjonen  og det faglege innhaldet i oppgåva di.  

## Kjelder til aktualisering og idémyldring 

### Nettsider 

“Nettside” er samleomgrep som i sin breiaste definisjon famnar om alt som er publisert på nettet.  

Sjølv om du har tilgang til mange av kjeldene dine via internett, er det ikkje sikkert dei sorterer under kjeldetypen “nettside”. Kan du plassere kjelda inn under ein annan kjeldetype, for eksempel vitskapleg artikkel, oppslagsverk, avisartikkel eller offentleg dokument? Då er det denne kjeldetypa du skal ta utgangspunkt i. Dette kan vere litt forvirrande! For eksempel er nrk.no ei nettside, men også ei nettavis. Store norske leksikon (snl.no) er ei nettside, men òg eit oppslagsverk.  

Du kan bruke som tommelfingerregel at sider som ikkje sorterer under andre kjeldetypar behandlast som nettsider. Søk & Skriv er eit eksempel, det same er faktisk.no eller nettsidene til biblioteket ditt. 

Nettsider kan vere truverdige og seriøse, heilt useriøse, eller med hensikt villeiande og konspiratoriske. Det kan vere alt frå pålitelege nettsider som for eksempel regjeringen.no, til personlege bloggar og nettsider laga av trollfabrikkar.Når du brukar nettsider som kjelde må du derfor [vurdere innhaldet kritisk](/kjeldebruk/kjeldevurdering.html)

::: tip Tips
_Nettsida [EUvsDisinfo](https://euvsdisinfo.eu/) er ein seriøs aktør der redaksjonen jobbar aktivt med å gi oversikt over saker med upåliteleg innhald._
:::

### Nyheiter 

Nyheiter kan brukast for å aktualisere eit emne og fortelje noko om korleis det blir omtalt i media. Historiske nyheiter kan også brukast for å belyse korleis eit fenomen, sak eller liknande har vore omtala. Men ver merksam på at sjølv om nyheiter skal følge ein standard for objektivitet og kritisk kjeldebruk, så kan dei likevel vere prega av politiske og/eller økonomiske interesser. 

### Kronikkar og debattinnlegg 

Kronikkar, debattinnlegg og leiarartiklar er eksempler på kjelder som ofte er prega av eit klart standpunkt.  

Kronikk er ein tekst av opplysande eller argumenterande art, og er vanleg og finne i aviser. Kronikkar er som oftast ikkje skrivne av tilsette i avisa, men av for eksempel  politikarar eller fagpersonar. For eksempel brukar ofte forskarar kronikkar til å formidle forskingsbasert kunnskap inn i den offentlege meiningsutvekslinga. Kronikken har til hensikt er å fremme ein ståstad og har dermed likskapstrekk med debattinnlegget. 

Debattinnlegg er, i skilnad til kronikkar, gjerne noko kortare og har mindre krav til fagleg innhald. Dei kan og vere meir personlege i stilen. Som ordet tilseier, er debattinnlegg ofte del av ei løpande meiningsutveksling.  

### Populærvitskaplege artiklar og bøker 

Populærvitskaplege artiklar kan vere publisert på nettet, for eksempel på [forskning.no](https://www.forskning.no/), kapittel i bøker eller i magasin som for eksempel Illustrert vitenskap. I ein populærvitskapleg artikkel blir vitskap formidla i ei journalistisk språkdrakt. Målsettinga er å gjere forsking tilgjengeleg for eit breiare publikum. Du kan bruke ein populærvitskapeleg artikkel for å få ei enkel innføring om eit aktuelt tema og få tips til vidare lesing. Ofte vil ein populærvitskapleg artikkel nemne fagpersonar og viktige omgrep i faget som kan hjelpe deg å søke vidare etter fagfellevurdert litteratur.  

### Grå litteratur  

Grå litteratur er materiale og forsking produsert av organisasjonar utanfor dei tradisjonelle kommersielle eller akademiske publiserings- og distribusjonskanalane. 

Dei kan ha høg kvalitet, og kan då brukast og refererast til som sakleg grunnlag for argument. Dei kan også brukast som kjelder til inspirasjon og for å aktualisere i ei oppgåve. 



**Forskingsrapportar** 

Forskingsrapportar produsert ved universitet eller anerkjente forskingsinstitutt (som for eksempel SINTEF, NORCE eller NIFU), presenterer ofte kvalitetssikra og aktuell forsking på spesifikke felt. Dermed kan desse brukast som kjelder i ein litteraturgjennomgang. Du kan også bruke dei som kjelde til sekundærdata, det vil seie data som andre har samla inn, i motsetning til primærdata (intervju og observasjon) som er data du samlar inn sjølv.  

Mange forskingsrapportar er produsert på oppdrag. Slik oppdragsforsking kan vere vinkla og fremje særinteresser, då bør du vere merksam på korleis. Ei gråsone her er rapportar produsert av tenketankar/tankesmier, konsulentbyrå og interesseorganisasjonar.  Slike rapportar bør du vere litt ekstra varsam med å bruke.  



**Offentlege rapportar, lovverk og forskrifter, lovforarbeid, og liknande**

Det eksisterer mange offentlege dokument og rapportar, både internasjonalt (for eksempel hos ulike instansar under FN, OECD og Europakommisjonen), og nasjonalt (for eksempel offentlege utredningar, stortingsmeldingar og lovforskrifter).  

Stortingsmeldingar, høyringar og liknande kjelder kan og halde høgt fagleg nivå, men dei er nært knytt til lovgjeving og kan derfor vere prega av ein politisk agenda. 

NOU-ar blir produsert på tema som er samfunnsmessig aktuelle og samlar gjerne mykje sekundærdata.  

For nokon fag, som for eksempel rettsvitskap, pedagogikk, barnevern og policy-studiar, er denne typen litteratur sentrale kjelder som set premiss for faga sin praksis og dei analyserast aktivt.  

Formålet med å bruke for eksempel rapportar kan vere å studere dei som ein del av statsforvaltninga, vere til inspirasjon for aktuelle tema, og dels for å underbygge argumentasjon, men då som eit tillegg til vitskapelege publikasjonar. Ver obs på oppdragsgjevar og avsendar for denne typen kjelder – dei har gjerne ein agenda!  



**Åpne forskningsdata og offentleg statistikk** 

Det finst ei rekke stader ein kan hente sekundærdata som kan analyserast og/eller brukast til å aktualisere oppgåva.   

Statistisk sentralbyrå (SSB) er eit fagleg uavhengig statleg organ med ansvar for innsamling, bearbeiding og formidling av offisiell statistikk i Norge. SSB tilbyr tilgang til statistikk og sekundærdata på [ssb.no](https://www.ssb.no/). [Les meir om tilgang til data frå SSB her.](https://www.ssb.no/data-til-forskning)


[DataverseNO](https://dataverse.no/) er eit nasjonalt, opent arkiv med forskingsdata. For å finne relevante data, kan du kan avgrense søk på datasett etter fagområde.   

[Zenodo](https://zenodo.org/) er et opent europeisk forskingsarkiv som blei utvikla på bestilling av EF (no EU) for å støtte open forsking og open deling av data. Her finn du ei stor mengd datasett frå ei rekke fagområde. 

::: warning Husk 
_Om du vil bruke ein tabell eller graf du ikkje har laga sjølv, er det viktig at du vurderer framstillinga kritisk. Kva data bygger framstillinga på? Har du tilgang til datamaterialet? Kven har laga framstillinga? Har dei noko å vinne på at datamaterialet blir tolka på ein viss måte?_
::: 

### Tekst frå KI-verktøy (ChatGPT, Copilot)

Ver merksam på at KI-verktøy som ChatGPT og Microsoft Copilot *ikkje reknast som kjelder*. Verktøya er laga for å generere tekst, ikkje for å hente informasjon, og er basert på språkmodellar som gjettar neste ord i ei setning. Dei er trena opp på tekstar som inneheld mykje informasjon, men er ikkje trena opp til å gje att informasjon korrekt. Verktøya er derfor betre eigna for formål der språkleg flyt og samanheng betyr meir enn at innhaldet er påliteleg.  


### Video, radio, podcast o.l. 

Video, radio og podcast og liknande kan også brukast for å aktualisere tema og sette det i kontekst. Du kan også bruke slike kjelder for å orientere deg i aktuelle debattar. Dersom det er fagekspertar som uttalar seg, bør du alltid sjekke dette opp mot skriftlege, kvalitetssikra kjelder. Ver spesielt merksam på at video og podcast kan vere reklamefinansiert. Du må vurdere relevans og kritisk vurdere innhaldet, slik du ville gjort med andre ikkje-vitskaplege kjelder. 

### Bilde
Nokre gonger kan det vere aktuelt å illustrere eit poeng i oppgåva med for eksempel eit bilde eller ein graf. Dersom du brukar eit fotografi for å underbygge faktaopplysningar, må du forsikre deg om at bildet er ekte. Prøv å finne ut så mykje som mogleg: Kven er fotografen?  I kva samanheng er bildet teke? Er det publisert av redaktørstyrte medium eller posta av ein privatperson? Nettstaden faktisk.no har fleire tips til korleis du kan faktasjekke bilde, [til dømes dette](https://www.faktisk.no/artikler/0rgve/slik-kan-du-finne-ut-om-et-bilde-har-blitt-faktasjekket) 

### Tidligare studentoppgåver (eigne eller andre sine)  
Bacheloroppgåver og andre mindre studentoppgåver kan brukast som inspirasjon. Du kan få nyttige tips om tema for oppgåva di, gode lesetips i kjeldelister, oppsett og struktur. Vi tilrår ikkje at du brukar slike oppgåver som faglege kjelder. 

::: warning Husk 
_Du skaper sjølv eit åndsverk når du skriv oppgåva di! Vel du å bruke ei oppgåve du har levert tidlegare, skal du vise til denne som eiga kjelde._
::: 

## Kjelder som gir oversikt og innføring 

### Lærebøker og innføringsbøker 

Ei lærebok er ein type tekst skrive for studentar som skal introduserast til eit nytt fag eller emne. Læreboka refererer teoriar og faghistorikk, der målet er å inkludere studentar i ein akademisk fellesskap med sin særeigne diskurs, dvs. eit fag sin talemåte. Ei fagbok gir innføring i kva faget dreier seg om, kva ein veit i faget og kva problemstillingar, omgrep og teoriar faget består av. Sjølv om ho også kan innehalde drøfting og argumentasjon, er det ikkje læreboka si oppgåve å argumentere for bestemte oppfatningar. Ver likevel merksam på at forfattaren gjerne legg mest vekt på dei teoriane som ligg han eller ho nærast som fagperson. 

### Leksikon, referansebøker, ensyklopedi  

Leksikon og oppslagsverk inneheld fagleg, kvalitetssikra informasjon. Innhaldet varierer frå korte innførslar til oversiktsartiklar som samanfattar sentral teori og forsking innanfor eit emne [(sjå også Handbooks/Companions)](/kjeldebruk/ulike-kjelder.html#handbooks-companions). Sistnemnde kan dermed vere ei viktig kjelde til tilvisingar. Ein leksikonartikkel er normalt ikkje argumenterande som ein [vitskapleg artikkel](/kjeldebruk/ulike-kjelder.html#vitskaplege-artiklar-tidsskrift-og-antologiar), eller like forklarande og instruerande som ei [lærebok](/kjeldebruk/ulike-kjelder.html#lærebøker-og-innføringsbøker).  

 

::: tip Tips 
_Treng du definisjonar til omgrep, ord og uttrykk, kan leksikon og oppslagsverk vere gode kjelder. Bruk gjerne fagspesifikke leksikon som for eksempel Store Medisinske Leksikon. Sjekk gjerne biblioteket si nettside for ditt fag for oversikt over gode oppslagsverk eller høyr med rettleiaren din eller tilsette ved ditt fagbibliotek._  
:::  

### Masteroppgåver 

Ei masteroppgåve er eit lengre vitskapeleg arbeid som er avgrensa til eit emne innanfor faget. Det er høgare krav til kunnskap om faget sine forskingsfunn, teoriar og metodar, samanlikna med andre studentoppgåver. Masteroppgåver har som formål å øve opp forskingskompetanse hos studenten, men det er ikkje et uttalt krav at masteroppgåva skal bidra med ny kunnskap til fagfeltet. Ei masteroppgåve kan derfor vere ei god kjelde til orientering og til vitskaplege referansar på eit tema.  

## Kjelder til fordjuping og grunngjeving

### Fagfellevurdering/peer review 

Det er forventa at vitskapelege kjelder har gjennomgått ei fagfellevurdering. Rutinar for dette varierer. Fagfellevurdering av ein vitskapleg artikkel inneber alltid at teksten blir vurdert av minst to uavhengige ekspertar innan faget før den blir akseptert for publisering. Artikkelforfattar får tilbakemelding på tekst og får konkrete forslag til forbetringar som må utførast før artikkelen eventuelt kan bli publisert. På denne måten blir artikkelen kvalitetssikra. Dette skal bidra til at artikkelen følger akademiske og vitskaplege normer som er gjeldande innanfor faget.  

### Vitskaplege artiklar (tidsskrift og antologiar)

Ein vitskapleg artikkel er ein tekst der forfattaren presenterer ny kunnskap eller gir nye perspektiv på eit fagleg spørsmål. Ein vitskapleg artikkel er først og fremst argumenterande: Han hevder at noko er tilfelle, eller sannsynlegvis tilfelle, og underbygger påstanden med evidens (argument, forskingsresultat). Ein vitskapeleg artikkel må derfor både ha gyldig argumentasjon og grundig dokumentasjon, slik at resultata kan etterprøvast.   

Forskingsartiklar vil ofte vere merka med formuleringar som «fagfellevurdert», «research article», «peer reviewed» «scholarly» eller liknande (les meir om kva det inneber under avsnittet [Fagfellevurdering/peer review](/kjeldebruk/ulike-kjelder.html#fagfellevurdering-peer-review)). Dersom artikkelen ikkje er merka, kan du vurdere om artikkelen presenterer ny kunnskap, eller om han tek utgangspunkt i kunnskap som er presentert tidlegare. Vurder også metodekapittelet for å sjå om funna kan etterprøvast. Er du framleis usikker, bør du rådføre deg med rettleiar. 

### Handbooks/Companions  

Handbooks og companions reknast som vitskaplege publikasjonar som bidreg med ny kunnskap i form av oppsummeringar av tilstanden i eit fagfelt, dvs. forskingsfronten eller “state of the art”. Desse kjeldene inneheld gjerne kapittel i form av autoritative oversiktsartiklar innan eit avgrensa område. Dei er gjerne skrivne av dei fremste fagekspertane, og samanfattar og drøftar sentral forsking og teori på feltet. Artiklane i handbooks utgjer dermed også ei viktig kjelde til andre sentrale referansar, og kan gi retning til framtidig forsking. 

Handbooks og companions er ikkje eit eintydig format, og vi har heller ikkje ei god norsk omsetjing for desse. 

### Fagbøker: Monografiar og antologiar 

Fagbøker er større vitskaplege bidrag eller studiar som er gjeve ut i form av ein monografi eller antologi. Monografiar er bøker skrivne av ein eller fleire forfattarar som står ansvarlege for alt innhaldet i boka. Antologiar er redigerte bøker som inneheld kapittel skrivne av ulike forfattarar, og kapitla er sett saman av redaktørane (*editors*) av boka. Fagbøker er skrivne for andre forskarar eller studentar.   

### Doktorgradsavhandlingar  

Ei avhandling er eit større, vitskapeleg arbeid. I mange fag er det vanlig å sette saman ei avhandling av allereie publiserte artiklar, med ei innleiing (kappe) som forklarar korleis artiklane inngår i den overordna samanhengen for avhandlinga. I andre fag, spesielt humanistiske fag, er det meir vanleg å skrive ein monografi, altså eit samanhengande arbeid som er inndelt i kapittel.  

Avhandlinga har fellestrekk med artikkelen. Den presenterer ny kunnskap til fagfolk,  argumenterer, har grundig dokumentasjon og  greier ut om metoden som er brukt.