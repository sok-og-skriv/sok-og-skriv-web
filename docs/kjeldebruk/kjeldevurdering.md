---
title: "Kjeldevurdering"
date: "2020-04-28"
lang: "nn"
tags: 
  - kildevurdering
  - kjeldevurdering
  - kildekritikk
  - kjeldekritikk
---
# Kjeldevurdering
Når du skal vurdere ei kjelde som formidlar kunnskap, må du ta omsyn til fleire faktorar: Kva type kjelde er det? Er kjelda truverdig? Kven står som ansvarleg? Andre spørsmål du bør stille deg er kven kjelda er produsert for, korleis ho refererer til andre og tidspunktet for publisering.  

Du må i tillegg gjere ei vurdering av om kjelda er relevant for ditt tema eller di problemstilling. Kva er det teksten ønsker å formidle? Kva er bodskapen? Les kjelda med di eiga problemstilling eller oppgåveformulering i tankane. Korleis kan kjelda vere med på å svare på *di* oppgåve? 

- Kva finn forfattaren i materialet sitt? 
- Kva materiale baserer forfattaren seg på? Kva type empiri ligg til grunn? 
- Har forfattaren tilstrekkeleg teoretisk og empirisk dokumentasjon for det hen hevdar? 
- Trekker forfattaren fram noko i materialet som særleg sentralt for argumentasjonen sin? 
- Ut frå kva fagleg perspektiv er kjelda skriven? 
 Sjå også [Lesemåtar](/studieteknikk/lesemater.html) for korleis du kan vurdere innhaldet i eit tekst.

 ## Er kjelda truverdig?

 ::: tip Tips
 _I denne videoen får du ei innføring med sjekkliste for kjeldevurdering._
 <Video id="LswBxnztpzU" title="Kildekritikk" />
 :::  

 ### Forfattar 
Når du skal vurdere ei kjelde, kan ein god start vere å sjå nærare på avsendaren, oftast ein forfattar. Blir forfattaren tydeleg presentert? Er det ein forfattar, eller er det fleire? Er det ein person, eller kanskje ein organisasjon? Kan du finne ut meir om bakgrunn og fagområde? Eit søk på forfattarnamn i [Oria](https://oria.no "Oria")eller Google kan gje nyttig informasjon.

### Utgjevar/ansvarleg

Utgjevar eller ansvarleg kan vere med på å påverke innhaldet. Det kan dreie seg om å fremme eigne interesser, eller å gjere seg attraktiv for potensielle lesarar. Kva blir utgjeve på forlaget eller i tidsskriftet? Sjå etter ei skildring av kva dei er interessert i å gje ut, og kva tema dei vanlegvis dekker. Undersøk også forlaget og tidsskriftet i [«Register over vitenskapelige publiseringskanaler»](https://kanalregister.hkdir.no/publiseringskanaler/Forside). Om forlaget eller tidsskriftet står oppført som anten nivå 1 eller nivå 2, kan du rekne med at det er eit seriøst og godt forlag eller tidsskrift. 

Tidsskrift på nivå 1 og 2 har fagfellevurdering. [Fagfellevurdering](/kjeldebruk/ulike-kjelder.html#fagfellevurdering-peer-review) betyr at ein artikkel blir vurdert av minst to fagpersonar innan same forskingsfelt før han blir akseptert for publisering. Artikkelforfattar får tilbakemelding på tekst og får konkrete forslag til forbetringar som må utførast før artikkelen eventuelt kan bli publisert. På denne måten blir artikkelen kvalitetssikra. Dette skal bidra til at artikkelen følger akademiske og vitskaplege normer som er gjeldande innanfor faget. 

### Aktualitet 
Sjekk publiseringstidspunkt for kjelda. Ofte er vi ute etter informasjon som er oppdatert, og som gjev den nyaste kunnskapen innan fagfeltet. Her vil det vere aktuelt å konsentrere seg om kjelder frå dei siste 5-10 åra. Andre gonger er vi ute etter eldre informasjon. Det kan vere informasjon publisert innanfor ein historisk periode eller i eit spesifikt år. Her kan det vere aktuelt med referansar publisert for meir enn 5-10 år sidan. 
For å sikre deg oppdatert kunnskap bør du skaffe siste utgåve av ei bok, dersom ho har kome i fleire utgåver. 

- Undersøk når boka blei gjeve ut ved å sjå kolofonsida i boka (oftast baksida av tittelbladet). 
- Undersøk kva utgåve det er. Er det ei ny og revidert utgåve, eller eit opptrykk/nytt opplag av tidlegare utgåve? 
- Undersøk datering på kjelda sine eigne referansar. 
- Brukar du ressursar på nett, bør du sjekke når informasjonen er publisert, eller sist oppdatert. 

### Relevans

Problemstillinga eller oppgåveformuleringa di vil vere styrande for i kva grad ei kjelde er relevant for deg. 

Start med å lese grundig gjennom teksten. For å gjere deg opp ei meining om relevans, må du vite noko om kva type tekst det er, og kven teksten er skriven for. 

### Målgruppe og sjanger
All tekst er skrive med tanke på ein lesar, der den tenkte lesaren er med på å forme teksten. Forskingsartiklar er skrivne med tanke på andre forskarar, gjerne innan same fagfelt. Ofte undersøker forskaren noko heilt spesifikt. Kva forskingsspørsmål tek forfattaren utgangspunkt i? Kva metode er brukt? Er resultata overførbare til ditt prosjekt?  

Ei lærebok er skriven for elevar eller studentar og har ofte ei breiare tilnærming. Kan lærebokteksten kaste lys over din spesifikke problemstilling, eller blir det for generelt?  Ofte vil du sjå at ei samansetting av ulike kjelder vil gjere seg i stand til å svare på spørsmåla i oppgåva di. 

Ver merksam på at sjølv om ei kjelde tar for seg eit tema som er relevant for deg, kan det vere tolka utifrå heilt andre rammeverk enn det som gjeld innanfor ditt fag.  

### Truverd

Når du les ein tekst, blir du i dei fleste tilfelle presentert for forfattaren sine synspunkt. I vitskaplege tekstar vil desse synspunkta vere basert på funn forfattaren har gjort i forskinga si. Vi som lesarar skal likevel ikkje utan vidare godta desse synspunkta. Vi må vurdere kritisk korleis forfattaren argumenterer for dei. 

Eit synspunkt vert støtta av påstandar, og slike støttande eller underbyggande påstandar blir kalla argument. Eit viktig krav vi stiller til argument, er at dei er haldbare. Forfattaren kan ikkje dikte opp innhaldet i eit argument, innhaldet må vere basert på fakta som kan observerast eller som fellesskapen av forskarar held for å vere sanne. For at eit argument skal vere haldbart, krevst det derfor at lesaren får tilgang til dokumentasjonen som argumentet bygger på. Når forfattaren viser til kjeldene sine eller presenterer metodeval og datamateriale, kan lesaren sjå med eigne auge om forfattaren har belegg for det han eller ho påstår. 

### Formål

Du kan vurdere hensikt ved å stille følgande sprsmål til teksten: Korleis formidlar forfattaren sitt materiale eller sine resultat? Kvifor er teksten skriven og kva formål har forfattaren hatt? Kva ønskjer forfattaren å formidle til deg som lesar?  

Du kan finne ut korleis forfattaren formidlar resultata eller slutningane sine ved å sjå på retoriske grep. Les med desse spørsmåla i tankane:

- Korleis er språket? 
- Korleis vil forfattaren overtyde deg om at perspektivet hen har er fruktbart?  
- Korleis får forfattaren deg til å eventuelt ignorere andre perspektiv ? 
- Blir eventuelle svakheiter i presentasjonen av forskinga synleggjort?  
- Kva gjer forfattaren for å halde på merksemda di?  
- Korleis blir du som lesar vekka? Kva eksempel vel forfattaren å vise fram? Er dei morosame? Urovekkjande eller opplysande?  
- Kva haldning har forfattaren til stoffet som blir presentert? Korleis blir du som lesar inkludert? Er det eit engasjement som smittar over på lesaren? Korleis er tonen i teksten?  
 
## Øving: Er desse tre gode kjelder? 

Du har funne tre interessante treff på eit søk på "helse og fysisk aktivitet". Korleis vurderer du desse tre kjeldene? 

Kva type kjelde er det? 
Er kjelda truverdig? 
Kven står som ansvarleg? 
Kva er det teksten ønskjer å formidle? 
Kva er bodskapen til kjelda?

:::: oppgave Kjelde 1
[**Helseeffekter av fysisk aktivitet**](https://www.fhi.no/ml/aktivitet/helseeffekter-av-fysisk-aktivitet/)

::: details Vårt vurderingforslag
Dette er ein artikkel publisert på folkehelseinstituttet si nettside i 2019. Her kan me vurdere utgjevar som truverdig og innhaldet som oppdatert. Artikkelen innleiier med å vise til tidlegare forsking og har ei fyldig referanseliste til sist. Her kunne det vere nyttig å lese artikkelen for å danne eg eit bilete av feltet for deretter å gå nærare inn i stoffet via forskinga det blir vist til. Både folkehelsesintituttet sin artikkel og referansar kan fungere som kjelder. Teksten er opplysande og bodskapen ser ut til å vere at det finst godt belegg for påstanden om at fysisk aktivitet er godt for helsa.
:::
::::


:::: oppgave Kjelde 2
[**Gjør du dette hver dag, forebygger du 30 sykdommer**](https://www.klikk.no/helse/gar-du-i-rask-gange-hver-dag-forebygger-du-mot-30-ulike-sykdommer-6817180)

::: details Vårt vurderingsforslag 
Dette er ei sak frå nettmagasinet klikk.no publisert i 2019. Saka viser til forsking, men har inga referanseliste. Dersom innhaldet er relevant kan det vere aktuelt å søke vidare på forskinga det blir vist til, men saka frå klikk.no fungerer dåleg som kjelde. Dersom du ønskjer å seie noko om dekninga av helsestoff i media kan  saka fungere som ein illustrasjon. Teksten er opplysande, men sett på spissen. Overskrifta og innleiinga er særleg spissa, og ein bør ta med i vurderinga at teksten er publisert i ein kommersiell kanal.
:::
::::

:::: oppgave Kjelde 3
[**Fysisk aktivitet og psykisk helse - Hvordan kan sykepleiere fremme fysisk aktivitet for mennesker med depresjon?**]( https://hdl.handle.net/11250/2653415)
::: details Vårt vurderingforslag
Dette er ei bacheloroppgåve frå 2019. Dersom oppgåva er relevant for ditt tema, kan referanselista vere nyttig å sjå nærare på. Som ein hovudregel tilrår me ikkje studentoppgåver som kjelder, unntaket er om det kjem fram heilt ny informasjon. Sjekk andre kjelder først og vurder kritisk om du treng å vise til bacheloroppgåva. 
:::
::::